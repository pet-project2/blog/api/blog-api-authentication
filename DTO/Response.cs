﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class Response
    {
        public bool isSuccess { get; set; }
        public string message { get; set; }
    }
}
