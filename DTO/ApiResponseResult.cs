﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DTO
{
    public class ApiResponseResult<T> : ApiResponse
    {
        public T result { get; set; }
    }
}
