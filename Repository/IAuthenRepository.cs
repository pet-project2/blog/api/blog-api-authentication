﻿using Domain;
using DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public interface IAuthenRepository
    {
        //Task<Response> CreateUserAsync(BlogUser user, string password);
        Task<Response> CreateUserAsync(BlogUser user, string password);
        Task<Response> DeleteUserAsync(BlogUser user);
        Task<bool> CheckUserExistAsync(string userName);
        Task<IEnumerable<BlogUser>> GetListUserAsync();
        Task<BlogUser> GetUserAsync(string userName);
    }
}
