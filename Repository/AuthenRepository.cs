﻿using Domain;
using DTO;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repository
{
    public class AuthenRepository : IAuthenRepository
    {
        private readonly UserManager<BlogUser> _userManager;
        private readonly RoleManager<BlogUserRole> _roleManager;
        public AuthenRepository(UserManager<BlogUser> userManager, RoleManager<BlogUserRole> roleManager)
        {
            this._userManager = userManager;
            this._roleManager = roleManager;
        }

        public async Task<bool> CheckUserExistAsync(string userName)
        {
            var user = await this.GetUserAsync(userName);
            if(user != null)
            {
                return true;
            }

            return false;
        }

        public async Task<Response> CreateUserAsync(BlogUser user, string password)
        {
            var result = await this._userManager.CreateAsync(user, password);
            return BaseResponse(result);
        }

        public async Task<Response> DeleteUserAsync(BlogUser user)
        {
            var result = await this._userManager.DeleteAsync(user);
            return BaseResponse(result);
        }

        public async Task<IEnumerable<BlogUser>> GetListUserAsync()
        {
            var listUser = await this._userManager.Users.ToListAsync();
            return listUser;
        }

        public async Task<BlogUser> GetUserAsync(string userName)
        {
            var user = await this._userManager.FindByEmailAsync(userName);
            return user;
        }

        private Response BaseResponse(IdentityResult result)
        {
            if (!result.Succeeded)
            {
                var error = result.Errors.Select(e => e.Description);
                return new Response
                {
                    isSuccess = false,
                    message = string.Join(", ", error.ToArray())
                };
            }

            return new Response
            {
                isSuccess = true,
                message = "Create new user successfully"
            };
        }
    }
}
