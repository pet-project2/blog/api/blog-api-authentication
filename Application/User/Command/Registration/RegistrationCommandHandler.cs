﻿using Domain;
using DTO;
using Mapster;
using MediatR;
using Repository;
using System.Threading;
using System.Threading.Tasks;

namespace Application.User.Command.Registration
{
    public class RegistrationCommandHandler : IRequestHandler<RegistrationCommand, Response>
    {
        private readonly IAuthenRepository _authenRepository;
        public RegistrationCommandHandler(IAuthenRepository authenRepository)
        {
            this._authenRepository = authenRepository;
        }

        public async Task<Response> Handle(RegistrationCommand request, CancellationToken cancellationToken)
        {
            var userName = request.Username.Trim().ToLower();
            var password = request.Password;
            var user = request.Adapt<BlogUser>();
            user.UserName = userName;
            user.Email = userName;
            var isUserExist = await this._authenRepository.CheckUserExistAsync(userName);
            if (!isUserExist)
            {
                var result = await this._authenRepository.CreateUserAsync(user, password);
                return result;
            }

            return new Response()
            {
                isSuccess = false,
                message = "Email is Exited"
            };
        }
    }
}
