﻿using DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Application.User.Command.Delete
{
    public class DeleteCommand : IRequest<Response>
    {
        [Required(ErrorMessage = "User Name is required.")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string UserName { get; set; }
    }
}
