﻿using DTO;
using MediatR;
using Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.User.Command.Delete
{
    public class DeleteCommandHandler : IRequestHandler<DeleteCommand, Response>
    {
        private readonly IAuthenRepository _authenRepository;
        public DeleteCommandHandler(IAuthenRepository authenRepository)
        {
            this._authenRepository = authenRepository;
        }
        public async Task<Response> Handle(DeleteCommand request, CancellationToken cancellationToken)
        {
            var userName = request.UserName;
            var result = await this._authenRepository.CheckUserExistAsync(userName);
            if(result)
            {
                var user = await this._authenRepository.GetUserAsync(userName);
                var deleteResult = await this._authenRepository.DeleteUserAsync(user);
                return deleteResult;
            }

            return new Response()
            {
                isSuccess = false,
                message = "User does not Existed"
            };
        }
    }
}
