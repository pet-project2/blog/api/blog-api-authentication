﻿using DTO;
using DTO.Account;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.User.Query.List
{
    public class ListUserQuery : IRequest<IEnumerable<UserDTO>>
    {
    }
}
