﻿using Domain;
using DTO;
using DTO.Account;
using Mapster;
using MediatR;
using Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.User.Query.List
{
    public class ListUserQueryHandler : IRequestHandler<ListUserQuery, IEnumerable<UserDTO>>
    {
        private readonly IAuthenRepository _authenRepository;
        public ListUserQueryHandler(IAuthenRepository authenRepository)
        {
            this._authenRepository = authenRepository;
        }
        public async Task<IEnumerable<UserDTO>> Handle(ListUserQuery request, CancellationToken cancellationToken)
        {
            var listUser = new List<UserDTO>();
            var result = await this._authenRepository.GetListUserAsync();
            foreach(var user in result)
            {
                var userDTO = user.Adapt<UserDTO>();
                listUser.Add(userDTO);
            }

            return listUser;
        }
    }
}
