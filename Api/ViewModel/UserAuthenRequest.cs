﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.ViewModel
{
    public class UserAuthenRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
