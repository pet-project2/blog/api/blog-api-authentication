﻿using Api.ViewModel;
using Application.User.Command.Delete;
using Application.User.Command.Registration;
using Application.User.Query.List;
using DTO;
using DTO.Account;
using IdentityModel.Client;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IMediator _mediator;
        public AccountController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        [HttpGet("getAll")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var query = new ListUserQuery();
                if (query == null || !ModelState.IsValid)
                {
                    var error = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage);
                    return BadRequest(new ApiResponse
                    {
                        status = 400,
                        isSuccess = false,
                        message = string.Join(", ", error.ToArray())
                    });
                }

                var result = await this._mediator.Send(query);
                return Ok(new ApiResponseResult<IEnumerable<UserDTO>>
                {
                    status = 200,
                    isSuccess = true,
                    message = "Get List User Successfully",
                    result = result
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse
                {
                    status = 400,
                    isSuccess = false,
                    message = ex.Message
                });
            }
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(RegistrationCommand command)
        {
            try
            {
                if (command == null || !ModelState.IsValid)
                {
                    var error = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage);
                    return BadRequest(new ApiResponse
                    {
                        status = 400,
                        isSuccess = false,
                        message = string.Join(", ", error.ToArray())
                    });
                }

                var result = await this._mediator.Send(command);
                if (result.isSuccess)
                {
                    return Ok(new ApiResponse
                    {
                        status = 201,
                        isSuccess = true,
                        message = "Register new user successfully"
                    });
                }

                return BadRequest(new ApiResponse
                {
                    status = 400,
                    isSuccess = false,
                    message = result.message
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new ApiResponse
                {
                    status = 400,
                    isSuccess = false,
                    message = ex.Message
                });
            }
        }

        [HttpDelete("delete/")]
        public async Task<IActionResult> Delete(DeleteCommand command)
        {
            if (command == null || !ModelState.IsValid)
            {
                var error = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage);
                return BadRequest(new ApiResponse
                {
                    status = 400,
                    isSuccess = false,
                    message = string.Join(", ", error.ToArray())
                });
            }

            var result = await this._mediator.Send(command);

            if (result.isSuccess)
            {
                return Ok(new ApiResponse
                {
                    status = 204,
                    isSuccess = true,
                    message = "Delete user successfully"
                });
            }

            return BadRequest(new ApiResponse
            {
                status = 400,
                isSuccess = false,
                message = result.message
            });
        }
    }
}
