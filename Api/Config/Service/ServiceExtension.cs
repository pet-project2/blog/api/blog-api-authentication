﻿using Domain;
using Entity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Config.Service
{
    public static class ServiceExtension
    {
        public static void AddIdentityConfig(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("Blog-User");
            services.AddDbContext<AuthenDbContext>(opts => opts.UseSqlServer(connectionString));

            services.AddIdentity<BlogUser, BlogUserRole>()
                    .AddEntityFrameworkStores<AuthenDbContext>()
                    .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                // Default Lockout settings.
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 4;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.User.RequireUniqueEmail = true;
            });
        }

        public static void AddContainerService(this IServiceCollection services)
        {
            services.AddScoped<IAuthenRepository, AuthenRepository>();
        }
    }
}
