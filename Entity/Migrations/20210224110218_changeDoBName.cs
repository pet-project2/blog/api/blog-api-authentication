﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class changeDoBName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DoB",
                table: "AspNetUsers",
                newName: "BirthDay");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BirthDay",
                table: "AspNetUsers",
                newName: "DoB");
        }
    }
}
