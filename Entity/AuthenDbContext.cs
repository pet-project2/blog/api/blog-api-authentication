﻿using Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace Entity
{
    public class AuthenDbContext : IdentityDbContext<BlogUser, BlogUserRole, Guid>
    {
        public AuthenDbContext(DbContextOptions options) : base(options)
        {

        }
    }
}
