﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Domain
{
    public class BlogUser : IdentityUser<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Bio { get; set; }
        public DateTime BirthDay { get; set; }
        public int Gender { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string Address { get; set; }
    }
}
